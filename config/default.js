const path = require('path')
const configs = {
  env:  "dev",
  mysql: {
    host: '127.0.0.1',
    user: 'root', //数据库用户名
    password: 'root', //数据库密码
    database: 'myblog', //数据库
    port: 3306, //端口
  },
  mongodb: {
    host: '127.0.0.1',
    user: 'root', //数据库用户名
    password: 'root', //数据库密码
    database: 'myblog', //数据库
    port: 27017, //端口
  },
  cors: { //跨域请求
    origin: ['http://localhost:3000', 'http://localhost:3001'],
  },
  pwd: { //密码
    salt: 10, //密码强度
  },
  token: { //token https://github.com/auth0/node-jsonwebtoken
    secretOrPrivateKey: 'iboomer', // app.superSecret key the secret for HMAC algorithms, or the PEM encoded private key for RSA and ECDSA.
  },
  email: { //默认邮箱
    service: "service@notice.xxxxx.com",
    spassword: "xxxx2019",
    admin: "xxxx@163.com",
  },
  page: { //默认分页
    currPage: 1,
    pageSize: 10,
  },
  file: { //文件配置
    path: { //路径
      upload: { //上传路径
        default: path.join(__dirname, '../public/attchments/default/'), //默认
        face: path.join(__dirname, '../public/attchments/face/'), //头像
      },
    },
    limit: { //限制
      fileSize: { //单个文件最大
        default: 10 * 1024 * 1024, //10MB
        face: 5 * 1024 * 1024, //5MB
      },
      maxCount: {
        default: 9,
        face: 1,
      },
    },
  },
  filePrefix: "http://localhost:3000/attchments/", //文件前缀
  fileAbsolute: { //文件绝对路径
    default: 'http://localhost:3000/attchments/default/',
    face: 'http://localhost:3000/attchments/face/',
  },
}

module.exports = configs;
