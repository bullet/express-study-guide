const mongoose = require('./connect/connect')
const postsSchema = require('./schemas/posts')

module.exports = mongoose.model('Post', postsSchema)
