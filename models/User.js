const mongoose = require('./connect/connect')
const usersSchema = require('./schemas/users')

// 表和schema关联,数据库中建立时默认会加s，即在数据库中呈现的是users,如果不想加s,在第三个参数上写上你指定的表名
module.exports = mongoose.model('User', usersSchema)
