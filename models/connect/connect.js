const mongoose = require('mongoose')
const configs = require('../../config/default')
const uri = 'mongodb://'+configs.mongodb.host+':'+configs.mongodb.port+'/'+configs.mongodb.database

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
// 让 mongoose 使用全局 Promise 库
mongoose.Promise = global.Promise
// 取得默认连接
const db = mongoose.connection

db.on('error', (err) => {
  console.error.bind(console, 'connection error:')
  console.log(err)
})

db.once('open', () => {
  console.log("Successful")
})

module.exports = mongoose
