const mongoose = require('mongoose')

const posts = new mongoose.Schema({
  // 标题
  title: {
    required: true,
    type: String
  },
  // 内容
  content: {
    required: true,
    type: String
  },
  // 作者id
  author_id: {
    required: true,
    type: mongoose.Types.ObjectId,
    ref: 'User'
  },
  // 发布日期
  create_at: {
    required: true,
    type: Date,
    default: Date.now()
  },
  // 更新日期
  update_at: {
    required: true,
    type: Date,
    default: Date.now()
  }
})
// Mongoose 会自动为每一个文档添加一个 __v 即 versionKey （版本锁）
module.exports = posts
