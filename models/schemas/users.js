const mongoose = require('../connect/connect')

// 用户表结构,对应数据库的 Collections
module.exports = new mongoose.Schema({
  // 用户名
  username: {
    required: true,
    type: String
  },
  // 密码
  password: {
    required: true,
    type: String
  },
  // 日期
  date: {
    required: true,
    type: Date,
    default: Date.now()
  }
})
