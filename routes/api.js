// 引入 express
const express = require('express')
const router = express.Router()
// 导入控制器
const PostCtr = require('../controllers/PostCtr')
const UserCtr = require('../controllers/UserCtr')
// 导入中间件
const UserMiddleware = require('../middlewares/UserMiddleware')
const PostMiddleware = require('../middlewares/PostMiddleware')

// 占位符传参,必须严格按照localhost/posts/xxx的形式访问,
// 如果少传一个参数就会报404错误，通过req.params.id获取对应的参数的值
router.get('/posts/:page', PostCtr.list)
router.get('/post/:post_id', PostMiddleware.postIdExist,PostCtr.detail)
router.post('/post/delete', [
  UserMiddleware.check,
  PostMiddleware.postIdExist
],PostCtr.delete)
router.post('/post/add', [
  UserMiddleware.check,
  PostMiddleware.postCheck
],PostCtr.add)
router.post('/post/edit', [
  UserMiddleware.check,
  PostMiddleware.postIdExist,
  PostMiddleware.postCheck
],PostCtr.edit)
router.post('/login', UserCtr.login)
router.post('/register', UserCtr.register)

module.exports = router
