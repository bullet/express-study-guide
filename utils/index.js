// 检查id是否是合法的mongodb的id
exports.checkObjecID = function (id) {
  return /^[a-fA-F0-9]{24}$/.test(id)
}
