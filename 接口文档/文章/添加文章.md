

## 接口名称

### 1) 请求地址

>http://localhost:3000/api/post/add

### 2) 调用方式：HTTP post

### 3) 接口描述：

* 接口描述详情

### 4) 请求参数:


#### POST参数:
|字段名称       |字段说明         |类型            |必填            |备注     |
| -------------|:--------------:|:--------------:|:--------------:| ------:|
|title||string|Y|-|
|content||string|Y|-|
|uid||string|Y|-|



### 5) 请求返回结果:

```
{
    "code": 200,
    "msg": "文章添加成功",
    "data": {
        "create_at": "2020-04-06T16:40:54.974Z",
        "update_at": "2020-04-06T16:40:54.974Z",
        "_id": "5e8c150d274f80affe8a78f2",
        "author_id": "5e89c02f3804e39019db2c5e",
        "title": "面对老赖欠钱不还,专家这样说",
        "content": "搜集证据交予法院,由法院强制执行,拒不执行,将列入失信名单,以后坐高铁飞机火车及其他的一些行为都将受到限制!",
        "__v": 0
    }
}
```


### 6) 请求返回结果参数说明:
|字段名称       |字段说明         |类型            |必填            |备注     |
| -------------|:--------------:|:--------------:|:--------------:| ------:|
|code||string|Y|-|
|msg||string|Y|-|
|data||string|Y|-|
|create_at||string|Y|-|
|update_at||string|Y|-|
|_id||string|Y|-|
|title||string|Y|-|
|author_id||string|Y|-|
|content||string|Y|-|
|__v||string|Y|-|

